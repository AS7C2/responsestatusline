//
//  AppDelegate.h
//  ResponseStatusLine
//
//  Created by Andrei Sherstniuk on 6/2/13.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
