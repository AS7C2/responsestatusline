//
//  ViewController.m
//  ResponseStatusLine
//
//  Created by Andrei Sherstniuk on 6/2/13.
//
//

#import <AFNetworking.h>
#import "ViewController.h"

@implementation ViewController

- (IBAction)action:(id)sender
{
	NSURL *url = [NSURL URLWithString:@"http://www.apple.com"];
	NSURLRequest *request = [NSURLRequest requestWithURL:url];
	AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
	[operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
	{
		NSHTTPURLResponse *response = [operation response];
		NSString *responseStatusLine = (__bridge_transfer NSString *)(CFHTTPMessageCopyResponseStatusLine((__bridge CFHTTPMessageRef)(response)));
		NSLog(@"responseStatusLine: %@", responseStatusLine);
	}
	failure:nil];
	[operation start];
}


@end
