//
//  main.m
//  ResponseStatusLine
//
//  Created by Andrei Sherstniuk on 6/2/13.
//
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
